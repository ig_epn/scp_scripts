#!/bin/bash
day=`date +"%Y.%m.%d"`
##Respaldar etc al servidor de datos 192.168.131.2 y a 192.168.1.161

directorio_respaldo_fisico="/respaldos/srvproscpp"

##copia de ETC semanal##
scp -pr /home/seiscomp/seiscomp/etc seiscomp@192.168.131.2:/mnt/WAVEFORMS/respaldos_scp/srvproscpp_etc/$day


##sincronizacion de scp semanal##
rsync -av /home/seiscomp/plugins_python seiscomp@192.168.1.161:/$directorio_respaldo_fisico/

scp -pr /home/seiscomp/seiscomp/etc  seiscomp@192.168.1.161:/$directorio_respaldo_fisico/seiscomp_etc_templates_var_lib/

scp -pr /home/seiscomp/seiscomp/share/templates/seedlink/q330  seiscomp@192.168.1.161:/$directorio_respaldo_fisico/seiscomp_etc_templates_var_lib/

scp -pr /home/seiscomp/seiscomp/share/templates/seedlink/reftek  seiscomp@192.168.1.161:/$directorio_respaldo_fisico/seiscomp_etc_templates_var_lib/


scp -pr /home/seiscomp/seiscomp/var/lib/seedlink/*  seiscomp@192.168.1.161:/$directorio_respaldo_fisico/seiscomp_etc_templates_var_lib/

