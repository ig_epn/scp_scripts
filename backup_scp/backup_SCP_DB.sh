#!/bin/bash

exec 1> >(logger -s -t $(basename $0)) 2>&1
##ESPECIFICAR EL HOME DEL USUARIO SEISCOMP

HOME="/home/seiscomp/"
##NO FUNCIONO HACER SOURCE
source $HOME/.bashrc

export SEISCOMP_ROOT="/home/seiscomp/seiscomp"
export PATH="/home/seiscomp/seiscomp/bin:/home/seiscomp/.local/bin:/home/seiscomp/programas/gsm:$PATH"
export LD_LIBRARY_PATH="/home/seiscomp/seiscomp/lib:$LD_LIBRARY_PATH"
export PYTHONPATH="/home/seiscomp/seiscomp/lib/python:/home/seiscomp/.local/lib/python3.8/:/home/seiscomp/programas/gsm:$PYTHONPATH"
export MANPATH="/home/seiscomp/seiscomp/share/man:$MANPATH"
source "/home/seiscomp/seiscomp/share/shell-completion/seiscomp.bash"



log=$HOME/scripts/scp_backup_db_result.log

salida=$HOME/respaldoDBSC3

usuarioDB="sysop"
claveBD="sysop"
host="localhost"
scp_db="seiscompt"

anio=`date +%Y`
mes=`date +%m --date='1 week ago'`
inicio="`date +%Y-%m --date='1 week ago'`-01 00:00:00"
final="`date +%Y-%m-%d` 00:00:00"

##Respaldar eventos de fechas especificas
#anio=2023
#mes="08"
#inicio="2023-08-01 00:00:00"
#final="2023-08-03 00:00:00"

rm -fr $salida/$anio 

echo " INICIO RESPALDO: $inicio "> $log

if [ ! -d $salida/$anio/$mes  ]
    then
    mkdir -p $salida/$anio/$mes
fi
for i in `scevtls --debug -d mysql://sysop:$claveBD@$host/$scp_db --begin "$inicio" --end "$final"`

do echo $i >> $log; scxmldump -d mysql://sysop:$claveBD@$host/$scp_db -E $i -PAMfF -o $salida/$anio/$mes/$i.xml;done 

echo "Inicio copia remota" &>> $log

scp -pr $salida/$anio/$mes seiscomp@192.168.131.2:/mnt/WAVEFORMS/respaldos_scp/eventos/$anio

scp -pr $salida/$anio/$mes seiscomp@192.168.1.161:/respaldos/srvproscpp/base_datos_scp_xml/$anio/

echo "Fin del respaldo $final" >>$log

