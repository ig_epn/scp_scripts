#!/bin/bash
log="$HOME/scripts/rsync.log"
echo "INICIO RSYNC ARCHIVE" > $log

SERVIDOR=192.168.131.2

ping -c 1 $SERVIDOR
if [ $? -eq 0 ]
then
echo "CONECTIVIDAD OK " >>$log
else
echo "FALLA CONECTIVIDAD" >>$log
exit
fi

remote_waveform_directory="/mnt/WAVEFORMS/"
local_waveform_directory="/data/archive"
#DATOS DE FECHA
ANIO=`date +%Y`
#ANIO=2016
#DJUL=$((`date +%j`-1))
#NETWORK=(`ls -1 $DSEISCOMP/$ANIO/`)

NETWORK=( EC CM OP CX PE VE )
#NETWORK=( CM )
for netw in "${NETWORK[@]}"
do
  echo "iniciando $netw" >>$log

  ESTACIONES=(`ls -1 $local_waveform_directory/$ANIO/$netw/`)

  for estacion in "${ESTACIONES[@]}"
    do

    rsync -a -v  --progress $local_waveform_directory/$ANIO/$netw/$estacion seiscomp@$SERVIDOR:$remote_waveform_directory/$ANIO/$netw/ >>$log


    done
done

